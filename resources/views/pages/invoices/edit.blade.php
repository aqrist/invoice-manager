@extends('layouts.sb')

@section('heading')
    Invoices
@endsection

@section('content')
    {{-- notification --}}
    @include('layouts/flash-message')
    {{-- end notification --}}

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card border-left-primary shadow">

                <div class="card-header">
                    Edit Invoice
                </div>

                <div class="card-body">
                    <form action="{{ route('invoices.update', $invoice->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <!-- Create row then inside there are 2 col md 6 -->
                        <div class="row mb-3">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="description">Description:</label>
                                    <input type="text" id="description" name="description"
                                        class="form-control @error('description') is-invalid @enderror"
                                        value="{{ $invoice->description }}" required>
                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="discount">Discount *on rupiah:</label>
                                    <input type="number" id="discount" name="discount"
                                        class="form-control @error('discount') is-invalid @enderror"
                                        value="{{ $invoice->discount }}">
                                    @error('discount')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>


                        </div>

                        <div class="form-group">
                            <button type="submit" name="save_only" class="btn btn-primary">Update</button>
                            <button type="submit" name="save_and_back" class="btn btn-info">Update and Back</button>
                        </div>

                    </form>
                </div>


            </div>
        </div>
    </div>
@endsection
