<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Invoice extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'billto',
        'billfrom',
        'invoice_number',
        'start_date',
        'due_date',
        'notes',
        'terms',
        'amount_paid',
        'discount',
        'status',
        'description',
    ];

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($invoice) {
            // Delete associated payments
            $invoice->payments()->delete();
            $invoice->items()->delete();
        });
    }
}
