<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Item;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (request()->ajax()) {
            # code...
            $query = Invoice::orderBy('created_at', 'DESC');

            // Check if the 'status' filter is set
            if (request('status') !== null) {
                $query->where('status', request('status'));
            }

            return Datatables::of($query)
                ->addColumn('total_amount', function ($item) {
                    $amount = Item::where('invoice_id', $item->id)->sum('price');
                    $discount = $item->discount;
                    $total_amount = ($amount - $discount);

                    return $total_amount;
                })
                ->addColumn('balance_due', function ($item) {
                    $subtotal = Item::where('invoice_id', $item->id)->sum('price');
                    $discount = $item->discount;
                    $amount_paid = Payment::where('invoice_id', $item->id)->sum('amount_paid');
                    $balance_due = ($subtotal - $discount) - $amount_paid;

                    return $balance_due;
                })
                ->editColumn('amount_paid', function ($item) {
                    $def = $item->amount_paid;
                    $total_payment = Payment::where('invoice_id', $item->id)->sum('amount_paid');
                    $grand_total = $def + $total_payment;

                    return $grand_total;
                })
                ->editColumn('status', function ($item) {
                    if ($item->status == '0') {
                        # code...
                        return '<i class="fas fa-fw fa-times"></i>';
                    } elseif ($item->status == '1') {
                        # code...
                        return '<i class="fas fa-fw fa-check"></i>';
                    }
                })
                ->addColumn('action', function ($item) {
                    return '
                    <a class="btn btn-sm btn-info m-1" href="' . route('invoices.edit', $item->id)  . '">
                        <i class="fas fa-fw fa-edit"></i>
                    </a>
                    <a class="btn btn-sm btn-info m-1" href="' . route('invoices.show', $item->id)  . '">
                        <i class="fas fa-fw fa-book"></i>
                    </a>
                    <a class="btn btn-sm btn-warning m-1" href="' . route('items.index', $item->id)  . '">
                        <i class="fas fa-fw fa-bookmark"></i>
                    </a>
                    <a class="btn btn-sm btn-success m-1" href="' . route('payments.index', $item->id)  . '">
                        <i class="fas fa-fw fa-dollar-sign"></i>
                    </a>
                    <a class="btn btn-sm btn-danger m-1" href="#" data-toggle="modal" data-target="#deleteModal' . $item->id . '">
                        <i class="fas fa-fw fa-trash"></i>
                    </a>

                    <!-- Delete Modal -->
                        <div class="modal fade" id="deleteModal' . $item->id . '" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel' . $item->id . '" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="deleteModalLabel' . $item->id . '">Confirm Deletion</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Are you sure you want to delete this item?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancel</button>
                                        <form method="POST" action="' . route('invoices.destroy', $item->id) . '" style="display: inline;">
                                            ' . csrf_field() . '
                                            ' . method_field('DELETE') . '
                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                            ';
                })
                ->rawColumns(['action', 'total_amount', 'status', 'balance_due'])
                ->make(true);
        }
        return view('pages.invoices.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.invoices.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validation logic goes here
        $validatedData = $request->validate([
            'billto' => 'required|string',
            'billfrom' => 'required|string',
            'start_date' => 'required|string',
            'due_date' => 'required|string',
            'notes' => 'required|string',
            'terms' => 'required|string',
            'discount' => 'nullable|string',
            'invoice_number' => 'string',
            'description' => 'string',
        ]);


        $validatedData['invoice_number'] = $this->generateInvoiceNumber();

        Invoice::create($validatedData);

        if ($request->has('save_only')) {
            # code to back and save
            return back()->with('success', 'Invoice added Succesfully!');
        } elseif ($request->has('save_and_back')) {
            # code to exit and save
            return redirect()->route('invoices.index')->with('success', 'Invoice added Succesfully!');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $invoice = Invoice::findOrFail($id);
        $items = Item::where('invoice_id', $id)->get();
        $subtotal = Item::where('invoice_id', $id)->sum('price');
        $paid = Payment::where('invoice_id', $id)->sum('amount_paid');

        $payments = Payment::where('invoice_id', $id)->get();

        return view('pages.invoices.show')->with([
            'invoice' => $invoice,
            'items' => $items,
            'subtotal' => $subtotal,
            'paid' => $paid,
            'payments' => $payments,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $invoice = Invoice::findOrFail($id);

        return view('pages.invoices.edit', compact('invoice'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $invoice = Invoice::findOrFail($id);

        $invoice->description = $request->input('description');
        $invoice->discount = $request->input('discount');

        $invoice->save();

        return redirect()->route('invoices.index')->with('success', 'Invoice updated Succesfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $invoice = Invoice::findOrFail($id);
            $invoice->delete();

            return redirect()->route('invoices.index')->with('success', 'Invoice deleted successfully');
        } catch (\Exception $e) {
            return redirect()->route('invoices.index')->with('error', 'Error deleting invoice, ' . $e . '');
        }
    }

    public function generateInvoiceNumber()
    {
        // Get the current year
        $year = date('Y');

        // Get the last invoice number from the database
        $lastInvoice = DB::table('invoices')->latest()->first();

        // Extract the sequential part of the last invoice number
        $lastSequential = $lastInvoice ? intval(substr($lastInvoice->invoice_number, -5)) : 0;

        // Increment the sequential part
        $newSequential = $lastSequential + 1;

        // Pad the sequential part with leading zeros
        $paddedSequential = str_pad($newSequential, 5, '0', STR_PAD_LEFT);

        // Create the new invoice number
        $invoiceNumber = "INV-$year-$paddedSequential";

        return $invoiceNumber;
    }

    public function markasdone($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice->status = true;
        $invoice->save();
        return back()->with('success', 'succesfully completed!');
    }
}
